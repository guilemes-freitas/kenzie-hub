import { Redirect, useHistory } from "react-router";
import Button from "../../components/Button";
import { Container, Content } from "./styles";
import LogoImage from "../../assets/logo.png";

function Home({ authenticated }) {
  const history = useHistory();

  const handleNavigation = (path) => {
    return history.push(path);
  };

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <img src={LogoImage} alt="" />
        <h1>Kenzie Hub</h1>
        <div>
          <Button onClick={() => handleNavigation("/login")}>Login</Button>
          <Button onClick={() => handleNavigation("/signup")} blueSchema>
            Cadastre-se
          </Button>
        </div>
      </Content>
    </Container>
  );
}

export default Home;
