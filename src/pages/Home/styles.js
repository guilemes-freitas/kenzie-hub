import styled, { keyframes } from "styled-components";
const appear = keyframes`
from{
    opacity: 0;
}

to{
    opacity: 1;
}
`;
export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  height: 100vh;
  animation: ${appear} 1s;
`;

export const Content = styled.div`
  max-width: 400px;
  h1 {
    font-size: 2.5rem;
    color: var(--purple);
  }
  div {
    flex: 1;
    margin-top: 1rem;
  }
`;
