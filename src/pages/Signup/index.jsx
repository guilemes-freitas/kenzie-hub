import { Link, Redirect, useHistory } from "react-router-dom";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { AnimationContainer, Background, Container, Content } from "./styles";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { toast } from "react-toastify";
import Select from "../../components/Select";

function Signup({ authenticated }) {
  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório!"),
    email: yup.string().email("Email inválido").required("Campo obrigatório!"),
    bio: yup.string().required("Campo obrigatório!"),
    contact: yup.string().required("Campo obrigatório!"),
    course_module: yup.string().required("Campo obrigatório!"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 digitos")
      .required("Campo obrigatório!"),
    passwordConfirm: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas estão diferentes")
      .required("Campo obrigatório!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const history = useHistory();

  const onSubmitFunction = ({
    name,
    email,
    bio,
    contact,
    course_module,
    password,
  }) => {
    const user = { name, email, bio, contact, course_module, password };
    console.log(user);
    api
      .post("/users", user)
      .then((_) => {
        toast.success("Sucesso ao criar a conta");
        return history.push("/login");
      })
      .catch((err) => toast.error("Erro ao criar a conta"));
  };

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunction)}>
            <h1>Cadastro</h1>
            <Input
              register={register}
              name="name"
              label="Nome"
              placeholder="Seu nome completo"
              error={errors.name?.message}
            ></Input>
            <Input
              register={register}
              name="email"
              label="Email"
              placeholder="Seu melhor email"
              error={errors.email?.message}
            ></Input>
            <Input
              register={register}
              name="bio"
              label="Bio"
              placeholder="Conte um pouco sobre você!"
              error={errors.bio?.message}
            ></Input>
            <Select
              register={register}
              name="course_module"
              label="Módulo do curso"
              placeholder="Módulo do curso"
              options={[
                {
                  value: "Primeiro módulo (Introdução ao Frontend)",
                  text: "Primeiro módulo",
                },
                {
                  value: "Segundo módulo (Frontend Avançado)",
                  text: "Segundo módulo",
                },
                {
                  value: "Terceiro módulo (Introdução ao Backend)",
                  text: "Terceiro módulo",
                },
                {
                  value: "Quarto módulo (Backend Avançado)",
                  text: "Quarto módulo",
                },
              ]}
              error={errors.course_module?.message}
            ></Select>
            <Input
              register={register}
              name="contact"
              label="Contato"
              placeholder="Seu número de contato ou rede social"
              error={errors.contact?.message}
            ></Input>
            <Input
              register={register}
              name="password"
              label="Senha"
              placeholder="Uma senha bem segura"
              type="password"
              error={errors.password?.message}
            ></Input>
            <Input
              register={register}
              name="passwordConfirm"
              label="Confirmação da senha"
              placeholder="Confirmação da senha"
              type="password"
              error={errors.passwordConfirm?.message}
            ></Input>
            <Button type="submit">Enviar</Button>
            <p>
              Já tem uma conta? Faça <Link to="/login">Login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
}

export default Signup;
