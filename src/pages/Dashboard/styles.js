import styled, { keyframes } from "styled-components";

const appear = keyframes`
from{
    opacity: 0;
}

to{
    opacity: 1;
}
`;

export const TechContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 1.5rem;
  justify-content: flex-start;
  align-items: center;
  padding: 0 38px;
  background-color: var(--blue);
  min-height: 300px;
  animation: ${appear} 1s;
`;

export const AddTechContainer = styled(TechContainer)`
  justify-content: space-around;
`;

export const Tech = styled.div`
  align-self: flex-start;
  padding: 10px;
`;
