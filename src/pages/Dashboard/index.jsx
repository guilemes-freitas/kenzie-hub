import { Redirect } from "react-router";
import api from "../../services/api";
import { useEffect, useState } from "react";
import Card from "../../components/Card";
import CreateTech from "../../components/CreateTech";
import { TechContainer, Tech, AddTechContainer } from "./styles";
import { toast } from "react-toastify";

function Dashboard({ authenticated }) {
  const [user, setUser] = useState([]);
  const [techs, setTechs] = useState([]);
  const [token] = useState(
    JSON.parse(localStorage.getItem("@Kenziehub:token")) || ""
  );
  const [userId] = useState(
    JSON.parse(localStorage.getItem("@Kenziehub:user_id")) || ""
  );

  function loadUser() {
    api
      .get(`/users/${userId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setUser(response.data);
        setTechs(response.data.techs);
        console.log(response.data);
      })
      .catch((err) => console.log(err));
  }

  const onSubmitFunction = ({ title, status }) => {
    api
      .post(
        "/users/techs",
        {
          title: title,
          status: status,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((_) => {
        toast.success("Sucesso ao adicionar a tecnologia!");
        loadUser();
      })
      .catch((err) => toast.error("Erro ao adicionar a tecnologia!"));
  };

  useEffect(() => {
    loadUser();
  }, []);

  const handleDelete = (id) => {
    const newTasks = techs.filter((tech) => tech.id !== id);
    api
      .delete(`/users/techs/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => setTechs(newTasks));
  };

  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  return (
    <>
      <AddTechContainer>
        <CreateTech onSubmitFunction={onSubmitFunction} />
      </AddTechContainer>
      {techs.length && (
        <TechContainer>
          {techs.map((tech) => {
            return (
              <Tech>
                <Card
                  key={tech.id}
                  tech={tech}
                  onClick={() => handleDelete(tech.id)}
                />
              </Tech>
            );
          })}
        </TechContainer>
      )}
    </>
  );
}

export default Dashboard;
