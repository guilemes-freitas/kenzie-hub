import { Redirect } from "react-router";
import { Container } from "./styles";

const OtherUsers = ({ authenticated }) => {
  if (!authenticated) {
    return <Redirect to="/login" />;
  }
  return (
    <Container>
      <h1>Ainda não implementado</h1>
      <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/dQw4w9WgXcQ?controls=0"
        title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
    </Container>
  );
};

export default OtherUsers;
