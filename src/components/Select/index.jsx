import { Container, SelectContainer } from "./styles";

function Select({ register, options, name, label, error, ...rest }) {
  return (
    <Container>
      <div>
        {label} {!!error && <span> - {error}</span>}
      </div>

      <SelectContainer isErrored={!!error}>
        <select {...register(name)} {...rest}>
          {options.map((option) => (
            <option key={option.text} value={option.value}>
              {option.text}
            </option>
          ))}
        </select>
      </SelectContainer>
    </Container>
  );
}

export default Select;
