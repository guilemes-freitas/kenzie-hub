import Input from "../Input";
import Button from "../Button";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import Select from "../Select";
import { Container } from "./styles";

const CreateTech = ({ onSubmitFunction }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório!"),
    status: yup.string().required("Campo obrigatório!"),
  });
  const [token] = useState(
    JSON.parse(localStorage.getItem("@Kenziehub:token")) || ""
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <Input
          label="Titulo"
          register={register}
          name="title"
          placeholder="Nome da tecnologia"
          error={errors.title?.message}
        ></Input>

        <Select
          register={register}
          name="status"
          label="Status"
          placeholder="Módulo do curso"
          options={[
            {
              value: "Iniciante",
              text: "Iniciante",
            },
            {
              value: "Intermediário",
              text: "Intermediário",
            },
            {
              value: "Avançado",
              text: "Avançado",
            },
          ]}
          error={errors.status?.message}
        ></Select>
        <Button type="submit" blueSchema>
          Adicionar
        </Button>
      </form>
    </Container>
  );
};

export default CreateTech;
