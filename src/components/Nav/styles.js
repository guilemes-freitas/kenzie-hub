import styled from "styled-components";

export const Container = styled.header`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 75px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  margin-bottom: 0.5rem;
`;

export const NavContainer = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-around;
  a {
    padding-left: 1rem;
  }
  img {
    width: 50px;
    padding: 10px;
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 150px;
  margin-right: 1rem;
  button {
    margin-top: 0;
  }
`;
