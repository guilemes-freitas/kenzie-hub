import { Link, useHistory } from "react-router-dom";
import Logo from "../../assets/logo.png";
import Button from "../Button";
import { Container, NavContainer, ButtonContainer } from "./styles";

const Nav = ({ setAuthenticated }) => {
  const history = useHistory();
  const handleDisconnect = () => {
    localStorage.clear();
    setAuthenticated(false);
    history.push("/");
  };
  return (
    <Container>
      <NavContainer>
        <img src={Logo} alt="logo"></img>
        <Link to="/dashboard">Tecnologias</Link>
        <Link to="/users">Kenzinhos</Link>
      </NavContainer>
      <ButtonContainer>
        <Button onClick={handleDisconnect}>Desconectar</Button>
      </ButtonContainer>
    </Container>
  );
};

export default Nav;
