import { useState } from "react";
import Button from "../Button";
import { Container } from "./styles";
// import Tilt from "react-tilt";

const Card = ({ tech, onClick }) => {
  const [selected, setSelected] = useState(false);

  const toggleClass = () => {
    setSelected(!selected);
  };

  const { title, status } = tech;
  return (
    <Container onClick={toggleClass}>
      <span> {title}</span>
      <hr />
      <span>{status}</span>
      <Button onClick={onClick}>Excluir</Button>
    </Container>
  );
};

export default Card;
