import styled from "styled-components";

export const Container = styled.button`
  background: ${(props) => (props.blueSchema ? "#1480fb" : "#6d0c91")};
  color: var(--white);
  height: 45px;
  border-radius: 5px;
  border: 2px solid var(--black);
  font-family: "Roboto Mono", monospace;
  margin-top: 16px;
  width: 100%;
  transition: 0.5s;

  :hover {
    border: 2px solid var(--orange);
  }
`;
