import { Container } from "./styles";
const Button = ({ children, blueSchema = false, ...rest }) => {
  return (
    <Container blueSchema={blueSchema} type="button" {...rest}>
      {children}
    </Container>
  );
};

export default Button;
