import { useEffect, useState } from "react";
import { Route, Switch } from "react-router";
import Nav from "../components/Nav";
import Dashboard from "../pages/Dashboard";
import Home from "../pages/Home";
import Login from "../pages/Login";
import OtherUsers from "../pages/OtherUsers";
import Signup from "../pages/Signup";

function Routes() {
  const [authenticated, setAuthenticated] = useState(false);
  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("@Kenziehub:token"));
    if (token) {
      return setAuthenticated(true);
    }
  }, [authenticated]);
  return (
    <Switch>
      <Route exact path="/">
        <Home authenticated={authenticated} />
      </Route>
      <Route exact path="/signup">
        <Signup authenticated={authenticated} />
      </Route>
      <Route exact path="/login">
        <Login
          authenticated={authenticated}
          setAuthenticated={setAuthenticated}
        />
      </Route>
      <Route exact path="/dashboard">
        <Nav setAuthenticated={setAuthenticated}></Nav>
        <Dashboard authenticated={authenticated}></Dashboard>
      </Route>
      <Route exact path="/users">
        <Nav setAuthenticated={setAuthenticated}></Nav>
        <OtherUsers authenticated={authenticated}></OtherUsers>
      </Route>
    </Switch>
  );
}

export default Routes;
